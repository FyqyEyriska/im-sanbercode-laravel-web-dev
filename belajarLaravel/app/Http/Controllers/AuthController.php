<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.register');
    }

    public function kirim(Request $request)
    {
        $FirstName = $request["fname"];
        $LastName = $request["iname"];

        return view('page.welcomee', ["FirstName" => $FirstName, 'LastName' => $LastName]);
    }
}
