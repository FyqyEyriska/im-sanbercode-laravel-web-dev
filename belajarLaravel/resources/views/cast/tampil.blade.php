@extends('layout.master')
@section('title')

Halaman Tampil Cast
@endsection
@section('content')

<a href="/cast/create" class="btn btn-primary btn-m my-2">Tambah</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">nama</th>
        <th scope="col">Ation</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>    
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
            </td>
        </tr>
         @empty
            
         @endforelse
      <tr>
      </tr>
    </tbody>
  </table>

@endsection