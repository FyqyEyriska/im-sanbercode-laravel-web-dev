<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'dashboard']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome', [AuthController::class, 'kirim']);

Route::get('master', function(){
    return view('layout.master');
});

Route::get('/data-tables', function(){
    return view('page.datatable');
});

Route::get('table', function(){
    return view('page.table');
});

// CRUD

// Create Data
// route untuk mengarah ke form cast
Route::get('/cast/create', [CastController::class, 'create']);
// route untuk menyimpan data inputan ke database table cast
Route::post('/cast', [CastController::class, 'store']);

// read data
// Route untuk menampilkan semua data yang ada di table cast database
Route::get('/cast',[CastController::class, 'index']);
// route untuk ambil detail
Route::get('/cast/{id}',[CastController::class, 'show']);

// update
// route
Route::get('/cast/{id}/edit',[CastController::class, 'edit']);
// berdasarkan id
Route::put('/cast/{id}', [CastController::class, 'update']);